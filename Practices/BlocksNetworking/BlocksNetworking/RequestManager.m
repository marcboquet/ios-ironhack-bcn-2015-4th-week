//
//  RequestManager.m
//  BlocksNetworking
//
//  Created by Daniel García García on 21/05/14.
//  Copyright (c) 2014 Produkt. All rights reserved.
//

#import "RequestManager.h"
#import "AFHTTPRequestOperationManager.h"

@interface RequestManager()

@end
@implementation RequestManager
- (instancetype)init
{
    self = [super init];
    if (self) {
        _baseDomain=@"http://ironhack4thweek.s3.amazonaws.com";
    }
    return self;
}
- (void)GET:(NSString *)path parameters:(id)parameters successBlock:(RequestManagerSuccess)successBlock errorBlock:(RequestManagerError)errorBlock{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:[self.baseDomain stringByAppendingPathComponent:path] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        successBlock(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        errorBlock(error);
    }];
}

@end
