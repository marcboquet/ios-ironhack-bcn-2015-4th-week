//
//  RequestManager.h
//  BlocksNetworking
//
//  Created by Daniel García García on 21/05/14.
//  Copyright (c) 2014 Produkt. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^RequestManagerSuccess)(id data);
typedef void (^RequestManagerError)(NSError *error);

@interface RequestManager : NSObject
@property (copy,nonatomic) NSString *baseDomain;
- (void)GET:(NSString *)path parameters:(id)parameters successBlock:(RequestManagerSuccess)successBlock errorBlock:(RequestManagerError)errorBlock;
@end
