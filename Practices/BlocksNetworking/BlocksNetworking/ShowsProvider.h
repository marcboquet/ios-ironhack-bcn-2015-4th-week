//
//  ShowsProvider.h
//  BlocksNetworking
//
//  Created by Daniel García García on 21/05/14.
//  Copyright (c) 2014 Produkt. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RequestManager.h"

@interface ShowsProvider : NSObject
- (void)showsWithSuccessBlock:(RequestManagerSuccess)successBlock errorBlock:(RequestManagerError)errorBlock;
@end
