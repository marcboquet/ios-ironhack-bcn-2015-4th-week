//
//  UserEntity.m
//  Shows
//
//  Created by Daniel García García on 19/05/14.
//  Copyright (c) 2014 Produkt. All rights reserved.
//

#import "UserEntity.h"


@implementation UserEntity

@dynamic userName;
@dynamic userPassword;

@end
