//
//  MoviesTableViewController.h
//  Models
//
//  Created by Daniel García García on 18/05/14.
//  Copyright (c) 2014 Produkt. All rights reserved.
//

#import <UIKit/UIKit.h>

@class UserEntity;
@interface MoviesTableViewController : UITableViewController
@property (strong,nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong,nonatomic) UserEntity *loggedUser;
@end
