//
//  LoginViewController.m
//  Shows
//
//  Created by Daniel García García on 19/05/14.
//  Copyright (c) 2014 Produkt. All rights reserved.
//

#import "LoginViewController.h"
#import "UserEntity.h"

@interface LoginViewController ()<UITextFieldDelegate>
@property (strong,nonatomic) NSManagedObjectContext *managedObjectContext;
@property (weak, nonatomic) IBOutlet UITextField *userNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (strong, nonatomic) IBOutletCollection(UITextField) NSArray *textFields;

- (IBAction)loginButtonAction:(id)sender;
@end

@implementation LoginViewController

- (id)initWithManagedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        _managedObjectContext=managedObjectContext;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self addViewControllerAsTexfieldsDelegate];
    [self.view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(backgroundTap:)]];
}

- (void)backgroundTap:(UITapGestureRecognizer *)gestureRecognizer{
    for (UITextField *textField in self.textFields) {
        [textField resignFirstResponder];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)loginButtonAction:(id)sender {
    if (self.userNameTextField.text.length && self.passwordTextField.text.length) {
        [self loginUserWithUsername:self.userNameTextField.text andPassword:self.passwordTextField.text];
    }else{
        NSLog(@"Invalid input");
    }
}

- (void)loginUserWithUsername:(NSString *)username andPassword:(NSString *)password{
    UserEntity *loggedUser=[self loggedUserWithUsername:username andPassword:password];
    if (!loggedUser) {
        loggedUser=[self addUserWithUsername:username andPassword:password];
    }
    if (loggedUser) {
        [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"lastLogin"];
        [[NSNotificationCenter defaultCenter] postNotificationName:UserDidLogNotification object:loggedUser];
        [self dismissViewControllerAnimated:YES completion:^{
            
        }];
    }
}
- (UserEntity *)loggedUserWithUsername:(NSString *)username andPassword:(NSString *)password{
    UserEntity *loggedUser;
    NSFetchRequest *request=[[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([UserEntity class])];
    request.predicate=[NSPredicate predicateWithFormat:@"userName=%@ AND userPassword=%@",username,password];
    NSArray *users=[self.managedObjectContext executeFetchRequest:request error:nil];
    if (users.count) {
        loggedUser=[users firstObject];
    }
    return loggedUser;
}
- (UserEntity *)addUserWithUsername:(NSString *)username andPassword:(NSString *)password{
    UserEntity *loggedUser=[NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([UserEntity class]) inManagedObjectContext:self.managedObjectContext];
    loggedUser.userName=username;
    loggedUser.userPassword=password;
    [self.managedObjectContext save:nil];
    return loggedUser;
}

#pragma mark - Text fields delegation
- (void)addViewControllerAsTexfieldsDelegate{
    self.userNameTextField.delegate = self;
    self.passwordTextField.delegate = self;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField == self.userNameTextField) {
        [self.passwordTextField becomeFirstResponder];
    }else if (textField == self.passwordTextField){
        [self.passwordTextField resignFirstResponder];
        [self loginButtonAction:nil];
    }
    return NO;
}
@end
