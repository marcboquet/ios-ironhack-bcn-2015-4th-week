//
//  CoreDataManager.h
//  Shows
//
//  Created by Daniel García on 24/06/14.
//  Copyright (c) 2014 Produkt. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CoreDataManager : NSObject
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
- (instancetype)initWithModelName:(NSString *)modelName;
@end
