//
//  MainTableViewController.m
//  Models
//
//  Created by Daniel García on 17/05/14.
//  Copyright (c) 2014 Produkt. All rights reserved.
//

#import "ShowsTableViewController.h"
#import "Show.h"
#import "NSString+Random.h"
#import "UserEntity.h"
#import "LoginViewController.h"

static NSString * const savedShowsFileName=@"shows.plist";

@interface ShowsTableViewController ()
@property (strong,nonatomic) NSMutableArray *shows;
@property (strong,nonatomic) NSMutableArray *likes;
@end

@implementation ShowsTableViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if(self){
        self.title=@"Shows";
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userDidLogin:) name:UserDidLogNotification object:nil];
    }
    return self;
}
- (NSMutableArray *)shows{
    if (!_shows) {
        _shows = [NSMutableArray array];
    }
    return _shows;
}
- (NSMutableArray *)likes{
    if (!_likes) {
        _likes = [NSMutableArray array];
    }
    return _likes;
}
- (void)loadLikes{
    NSString *savesFilePath = [self savesFilePath];
    NSDictionary *likesDictionary = [NSDictionary dictionaryWithContentsOfFile:savesFilePath];
    if (likesDictionary) {
        self.likes = [[likesDictionary valueForKey:@"likes"] mutableCopy];
    }
}
- (void)loadRemoteTVShows{
    NSURL *showsFeedURL = [NSURL URLWithString:@"http://ironhack4thweek.s3.amazonaws.com/shows.json"];
    NSData *showsData = [NSData dataWithContentsOfURL:showsFeedURL];
    NSError *error;
    NSDictionary *showsDictionary = [NSJSONSerialization JSONObjectWithData:showsData options:NSJSONReadingMutableContainers error:&error];
    for (NSDictionary *showDictionary in [showsDictionary valueForKey:@"shows"]) {
        Show *show=[[Show alloc] init];
        show.showId=[showDictionary valueForKey:@"id"];
        show.showTitle=[showDictionary valueForKey:@"title"];
        show.showDescription=[showDictionary valueForKey:@"description"];
        show.showRating=arc4random()/10.0f;
        [self.shows addObject:show];
    }
}
- (void)userDidLogin:(NSNotification *)notification{
    [self updateTitleWithLoggedUser:[self loggedUser]];
}
- (UserEntity *)loggedUser{
    UserEntity *loggedUser;
    NSFetchRequest *request=[[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([UserEntity class])];
    request.predicate=[NSPredicate predicateWithFormat:@"1=1"];
    NSArray *users=[self.managedObjectContext executeFetchRequest:request error:nil];
    if (users.count) {
        loggedUser=[users firstObject];
    }
    return loggedUser;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIBarButtonItem *saveShowsButton=[[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStylePlain target:self action:@selector(saveShows:)];
    self.navigationItem.leftBarButtonItem = saveShowsButton;
    
    UIBarButtonItem *addShowButton=[[UIBarButtonItem alloc] initWithTitle:@"Add" style:UIBarButtonItemStylePlain target:self action:@selector(addShow:)];
    self.navigationItem.rightBarButtonItem = addShowButton;
    
    [self loadLikes];
    [self loadRemoteTVShows];
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if (![self loggedUser]) {
        [self presentLoginView];
    }
}
- (void)presentLoginView{
    LoginViewController *loginViewController = [[LoginViewController alloc]initWithManagedObjectContext:self.managedObjectContext];
    [self.tabBarController presentViewController:loginViewController animated:YES completion:^{
        
    }];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self updateTitleWithLoggedUser:[self loggedUser]];
}
- (void)updateTitleWithLoggedUser:(UserEntity *)user{
    self.navigationItem.title=user?[NSString stringWithFormat:@"Shows (%@)",user.userName]:@"Shows";
}

- (void)addShow:(id)sender{
    [self.shows addObject:[self randomShow]];
    [self.tableView reloadData];
}

- (Show *)randomShow{
    Show *show=[[Show alloc] init];
    show.showId=[NSString randomString];
    show.showTitle=[NSString randomString];
    show.showDescription=[NSString randomString];
    show.showRating=arc4random()/10.0f;
    return show;
}

- (NSString *)archivePath{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    return [documentsDirectory stringByAppendingPathComponent:savedShowsFileName];
}

- (void)saveShows:(id)sender{
    if (self.shows.count) {
        NSData *showsData=[NSKeyedArchiver archivedDataWithRootObject:self.shows];
        NSDictionary *showsSaveData=@{@"save date":[NSDate date],@"shows array":showsData};
        [showsSaveData writeToFile:[self archivePath] atomically:YES];
        NSLog(@"Saved File to %@",[self archivePath]);
    }
}

- (void)loadShows{
    NSDictionary *showsSavedData=[NSDictionary dictionaryWithContentsOfFile:[self archivePath]];
    if ([showsSavedData valueForKey:@"shows array"]) {
        NSArray *shows=[NSKeyedUnarchiver unarchiveObjectWithData:[showsSavedData valueForKey:@"shows array"]];
        NSLog(@"Loaded shows saved at %@",[showsSavedData valueForKey:@"save date"]);
        self.shows=[shows mutableCopy];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.shows.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"reuseIdentifier"];
    if (!cell) {
        cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"reuseIdentifier"];
    }
    Show *show=[self.shows objectAtIndex:indexPath.item];
    cell.textLabel.text=show.showTitle;
    cell.detailTextLabel.text=show.showDescription;
    cell.accessoryType = [self.likes containsObject:show.showId]?UITableViewCellAccessoryCheckmark:UITableViewCellAccessoryNone;
    return cell;
}

#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    Show *show=[self.shows objectAtIndex:indexPath.item];
    if (![self.likes containsObject:show.showId]) {
        [self.likes addObject:[show.showId copy]];
    }else{
        for (NSString *showId in [self.likes copy]) {
            if ([showId isEqualToString:show.showId]) {
                [self.likes removeObject:showId];
            }
        }
    }
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    [self saveLikes];
}

- (void)compareWithFirstShow:(Show *)show{
    Show *firstShow=[self.shows firstObject];
    if ([firstShow isEqualToShow:show]) {
        [[[UIAlertView alloc]initWithTitle:@"Equal Show" message:@"It is equal !!" delegate:nil cancelButtonTitle:@"Close" otherButtonTitles: nil]show];
    }else{
        [[[UIAlertView alloc]initWithTitle:@"Equal Show" message:@"It is NOT equal" delegate:nil cancelButtonTitle:@"Close" otherButtonTitles: nil]show];
    }
}

- (void)findShow:(Show *)show{
    if ([self.shows containsObject:show]) {
        [[[UIAlertView alloc]initWithTitle:@"Find Show" message:@"Found !" delegate:nil cancelButtonTitle:@"Close" otherButtonTitles: nil]show];
    }else{
        [[[UIAlertView alloc]initWithTitle:@"Find Show" message:@"Not in dataSource" delegate:nil cancelButtonTitle:@"Close" otherButtonTitles: nil]show];
    }
}
- (NSString *)savesFilePath{
    UserEntity *loggedUser = [self loggedUser];
    NSString *savesFilePath;
    if (loggedUser) {
        NSString *cachesPath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) firstObject];
        savesFilePath = [cachesPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_saves.plist",loggedUser.userName]];
    }
    return savesFilePath;
}
- (void)saveLikes{
    NSString *savesFilePath = [self savesFilePath];
    if (savesFilePath) {
        [@{@"likes":self.likes} writeToFile:savesFilePath atomically:YES];
    }
}
@end
