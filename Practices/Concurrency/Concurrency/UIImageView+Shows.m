//
//  UIImageView+Shows.m
//  Concurrency
//
//  Created by Daniel García García on 26/05/14.
//  Copyright (c) 2014 Produkt. All rights reserved.
//

#import "UIImageView+Shows.h"
#import "ImageDownloader.h"
#import <objc/runtime.h>


@interface UIImageView (_Shows)
@property (strong,nonatomic,readonly) NSOperationQueue *operationQueue;
@end
@implementation UIImageView (Shows)
- (void)setImageWithURL:(NSURL *)imageURL completion:(void (^)(BOOL success))completion{
    self.image=nil;
    if (self.operationQueue.operations.count) {
        NSLog(@"cancel Operations");
        [self.operationQueue cancelAllOperations];
    }
    
    NSBlockOperation *operation = [NSBlockOperation blockOperationWithBlock:^{
        __block BOOL finished = NO;
        [[ImageDownloader defaultDownloader]downloadImageWithURL:imageURL completion:^(UIImage *image) {
            if (image) {
                self.image = image;
            }
            completion(image?YES:NO);
            finished = YES;
        }];
        while (!finished) {
            @autoreleasepool {
                NSDate *date = [NSDate dateWithTimeIntervalSinceNow:0.1];
                [[NSRunLoop currentRunLoop] runUntilDate:date];
            }
            
        }
    }];
    [self.operationQueue addOperation:operation];

    
    
}
- (NSOperationQueue *)operationQueue{
    NSOperationQueue *operationQueue = objc_getAssociatedObject(self, @"operationQueueKey");
    if (!operationQueue) {
        operationQueue = [[NSOperationQueue alloc] init];
        objc_setAssociatedObject(self, @"operationQueueKey", operationQueue, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
    return operationQueue;
}
@end
