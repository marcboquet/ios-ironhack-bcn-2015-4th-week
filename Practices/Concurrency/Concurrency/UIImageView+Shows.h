//
//  UIImageView+Shows.h
//  Concurrency
//
//  Created by Daniel García García on 26/05/14.
//  Copyright (c) 2014 Produkt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (Shows)
- (void)setImageWithURL:(NSURL *)imageURL completion:(void (^)(BOOL success))completion;
@end
