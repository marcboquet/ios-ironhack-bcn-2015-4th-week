//
//  ShowDetailViewController.h
//  BlocksNetworking
//
//  Created by Daniel García García on 21/05/14.
//  Copyright (c) 2014 Produkt. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Show;
@interface ShowDetailViewController : UIViewController
- (id)initWithShow:(Show *)show;
@end
