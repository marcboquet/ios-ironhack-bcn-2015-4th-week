//
//  MainTableViewController.m
//  Models
//
//  Created by Daniel García on 17/05/14.
//  Copyright (c) 2014 Produkt. All rights reserved.
//

#import "ShowsTableViewController.h"
#import "Show.h"
#import "NSString+Random.h"
#import "BlockButtonItem.h"
#import "ShowDetailViewController.h"
#import "ShowsProvider.h"
#import "DefaultTableViewCell.h"
#import "UIImageView+Shows.h"

static NSString * const savedShowsFileName=@"shows.plist";

@interface ShowsTableViewController ()
@property (strong,nonatomic) BlockButtonItem *addShowButton;
@property (strong,nonatomic) NSMutableArray *shows;
@property (strong,nonatomic) ShowsProvider *showsProvider;
@end

@implementation ShowsTableViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if(self){
        self.title=@"Shows";
        _shows=[NSMutableArray array];
        _showsProvider=[[ShowsProvider alloc] init];
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([DefaultTableViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([DefaultTableViewCell class])];
    
    UIBarButtonItem *saveShowsButton=[[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStylePlain target:self action:@selector(saveShows:)];
    self.navigationItem.leftBarButtonItem = saveShowsButton;
    [self loadShows];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

- (NSString *)archivePath{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    return [documentsDirectory stringByAppendingPathComponent:savedShowsFileName];
}

- (void)saveShows:(id)sender{
    if (self.shows.count) {
        NSData *showsData=[NSKeyedArchiver archivedDataWithRootObject:self.shows];
        NSDictionary *showsSaveData=@{@"save date":[NSDate date],@"shows array":showsData};
        [showsSaveData writeToFile:[self archivePath] atomically:YES];
        NSLog(@"Saved File to %@",[self archivePath]);
    }
}

- (void)loadShows{
    [self.showsProvider showsWithSuccessBlock:^(id data) {
        self.shows=data;
        [self.tableView reloadData];
    } errorBlock:^(NSError *error) {
        
    }];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.shows.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DefaultTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([DefaultTableViewCell class]) forIndexPath:indexPath];
    Show *show=[self.shows objectAtIndex:indexPath.item];
    cell.titleLabel.text=show.showTitle;
    cell.descriptionLabel.text=show.showDescription;
    
    [cell.thumbnailImageView setImageWithURL:show.posterImageURL completion:^(BOOL success) {
        
    }];

    return cell;
}

#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    Show *show=[self.shows objectAtIndex:indexPath.item];
    
    __block Show *selectedShow;
    [self.shows enumerateObjectsUsingBlock:^(Show *obj, NSUInteger idx, BOOL *stop) {
        if ([show isEqualToShow:obj]) {
            selectedShow=obj;
        }
    }];
    
    ShowDetailViewController *detailViewController=[[ShowDetailViewController alloc] initWithShow:selectedShow];
    [self.navigationController pushViewController:detailViewController animated:YES];
}
@end
