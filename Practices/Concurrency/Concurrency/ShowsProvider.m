//
//  ShowsProvider.m
//  BlocksNetworking
//
//  Created by Daniel García García on 21/05/14.
//  Copyright (c) 2014 Produkt. All rights reserved.
//

#import "ShowsProvider.h"
#import "Show.h"

@interface ShowsProvider()
@property (strong,nonatomic) RequestManager *requestManager;
@end
@implementation ShowsProvider
- (instancetype)init
{
    self = [super init];
    if (self) {
        _requestManager=[[RequestManager alloc] init];
    }
    return self;
}
- (void)showsWithSuccessBlock:(RequestManagerSuccess)successBlock errorBlock:(RequestManagerError)errorBlock{
    NSString *path=@"shows.json";
    NSDictionary *parameters=@{};
    [self.requestManager GET:path parameters:parameters successBlock:^(id data) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            NSMutableArray *shows=[NSMutableArray array];
            if ([data valueForKey:@"shows"] && ((NSArray *)[data valueForKey:@"shows"]).count) {
                for (NSDictionary *showDictionary in [data valueForKey:@"shows"]) {
                    Show *show=[[Show alloc] init];
                    show.showId=[showDictionary valueForKey:@"id"];
                    show.showTitle=[showDictionary valueForKey:@"title"];
                    show.showDescription=[showDictionary valueForKey:@"description"];
                    show.posterImageURL=[NSURL URLWithString:[showDictionary valueForKey:@"posterURL"]];
                    [shows addObject:show];
                }
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                successBlock(shows);
            });
        });
    } errorBlock:^(NSError *error) {
        errorBlock(error);
    }];
}
@end
