//
//  Product.h
//  Models
//
//  Created by Daniel García on 17/05/14.
//  Copyright (c) 2014 Produkt. All rights reserved.
//

#import "MTLModel.h"

@interface Show : MTLModel
@property (copy,nonatomic) NSString *showId;
@property (copy,nonatomic) NSString *showDescription;
@property (copy,nonatomic) NSString *showTitle;
@property (assign,nonatomic) CGFloat showRating;
@property (strong,nonatomic) NSURL *posterImageURL;

- (BOOL)isEqualToShow:(Show *)show;
@end    
