# Concurrency

## Exercise 1

- Add UIImageView to shows table

- Download show image using [NSData dataWithContentsOfURL:] 

- Show's image used for the exercise aren't too heavy. Let's use sleep() function to simulate a longer operation.

- Check how table scroll framerate drops down, because we are blocking the main thread.

- Move image download operations, to a UIImageView category

- Use GCD APIs to execute download and UIImage composing operations to a background queue

- Use GCD APIs to parse API data on a background thread

- Remember all UIKit operations MUST be executed on main queue


## Exercise 2

- Create a serial dispatch\_queue for data processing

- Create a singleton class ImageDownloader

- Move image downloading to ImageDownloader and implement an image cache (Use NSFileManager to save, check if cached image exists, and load cached images)


## Exercise 3

- Modify RequestManager to return an NSOperation 

- Modify UIImageView category to use NSOperation / NSOperationQueue to handle image download and setting

- Use runtime API to store the download operation in the UIImageView category

- Cancel the current download operation before adding a new one 





