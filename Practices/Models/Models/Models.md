# Models

## Exercise 1

- Create an iPhone application using UITabBarController with two sections. Each section would be UITableViewControllers that would present TVShows and Movies. 

- Create two entity classes. A class for representing TVShows and another one for representing Movies

- Implement NSCopying on both classes

- Populate each table with TVShows and Movies generated randomly (content is irrelevant for this practice. I could be random strings)

- Implement some UI that allow the user to duplicate one of the previously created entities class. 

- Implement NSCoding on both classes

- Save all entities to disk using NSKeyedArchiver (suggestion: when switching sections)

- Recover all saved data using NSKeyedUnarchiver




## Exercise 2

- Use previous project to implement Equality on TVShows and Movies classes

- Implement hash on both classes

- Implement custom Equality methods

- Implement some UI that allow the user to compare between entities. (duplicated entities must match and others must mismatch)

- Implement some UI that allow the user to search into the datasource for it (containsObject:)



## Exercise 3

- Fork your previous project to redo everything using Mantle

- Add Mantle using CocoaPods

- Remove Equality, NSCoding & NSCopying implementation and make all entities extend MTLModel



## Exercise 4

- Implement MTLJSONSerializing protocol

- Create a new class to read from JSON and return your entities populated with data

- Load an example JSON (provided by teacher) and obtain TVShows



