# 4th week of IronHack iOS Bootcamp Course 2015 in Barcelona.#

### What is this repository for? ###

Here you will find all the slides and sample projects for the 4th week of the course. This would be useful if you are taking the course.

Sample projects are meant only to explain some specific topics. For simplicity's sake, we have ignored some design patterns. Don't look them as an example of how to design an application, but how to use some libraries or APIs examples. 


### Presentations ###

In the Presentations folder you will find one presentation for each day of the week, one topic (sometimes two) per day. 

These presentations are written in Markdown and are meant to be opened using [Deckset App](http://www.decksetapp.com) (Better if you watch it using "Plane Jane" theme). 
You should get it if you don't have yet. Is Awesome. 

I will upload pdf exports for each presentation, for easier reading.