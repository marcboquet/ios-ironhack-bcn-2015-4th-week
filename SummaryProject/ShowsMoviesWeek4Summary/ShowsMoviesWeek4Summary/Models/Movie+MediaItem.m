//
//  Movie+MediaItem.m
//  ShowsMoviesWeek4Summary
//
//  Created by Daniel García on 29/06/14.
//  Copyright (c) 2014 Produkt. All rights reserved.
//

#import "Movie+MediaItem.h"
#import "MovieCellDrawer.h"
#import "MovieDetailViewController.h"

@implementation Movie (MediaItem)
- (NSString *)itemID{
    return self.movieID;
}
- (NSString *)itemTitle{
    return self.movieTitle;
}
- (id<CellDrawerProtocol>)cellDrawer{
    return [[MovieCellDrawer alloc] init];
}
- (UIViewController *)detailViewController{
    return [[MovieDetailViewController alloc] initWithMovie:self];
}
@end
