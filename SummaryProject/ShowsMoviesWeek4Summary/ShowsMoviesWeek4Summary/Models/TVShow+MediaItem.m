//
//  TVShow+MediaItem.m
//  ShowsMoviesWeek4Summary
//
//  Created by Daniel García on 27/06/14.
//  Copyright (c) 2014 Produkt. All rights reserved.
//

#import "TVShow+MediaItem.h"
#import "TVShowCellDrawer.h"
#import "TVShowDetailViewController.h"

@implementation TVShow (MediaItem)
- (NSString *)itemID{
    return self.showId;
}
- (NSString *)itemTitle{
    return self.showTitle;
}
- (id<CellDrawerProtocol>)cellDrawer{
    return [[TVShowCellDrawer alloc] init];
}
- (UIViewController *)detailViewController{
    return [[TVShowDetailViewController alloc] initWithShow:self];
}
@end
