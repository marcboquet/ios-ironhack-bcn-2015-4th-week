//
//  Movie+MediaItem.h
//  ShowsMoviesWeek4Summary
//
//  Created by Daniel García on 29/06/14.
//  Copyright (c) 2014 Produkt. All rights reserved.
//

#import "Movie.h"
#import "MediaItemProtocol.h"

@interface Movie (MediaItem)<MediaItemProtocol>

@end
