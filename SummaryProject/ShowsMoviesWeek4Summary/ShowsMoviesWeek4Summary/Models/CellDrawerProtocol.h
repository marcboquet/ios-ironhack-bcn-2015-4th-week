//
//  CellDrawerProtocol.h
//  ShowsMoviesWeek4Summary
//
//  Created by Daniel García on 27/06/14.
//  Copyright (c) 2014 Produkt. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol MediaItemProtocol;
@protocol CellDrawerProtocol <NSObject>
- (UITableViewCell *)cellForTableView:(UITableView *)tableView atIndexPath:(NSIndexPath *)indexPath;
- (void)drawCell:(UITableViewCell *)cell withItem:(id<MediaItemProtocol>)item;
@end
