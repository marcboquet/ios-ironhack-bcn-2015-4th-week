//
//  Movie.h
//  ShowsMoviesWeek4Summary
//
//  Created by Daniel García on 27/06/14.
//  Copyright (c) 2014 Produkt. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Mantle/Mantle.h>

@interface Movie : MTLModel<MTLJSONSerializing>
@property (copy,nonatomic) NSString *movieID;
@property (copy,nonatomic) NSString *movieTitle;
@property (copy,nonatomic) NSDate *publishDate;
@property (copy,nonatomic) NSString *posterURL;
@end
