//
//  MovieTableViewCell.m
//  ShowsMoviesWeek4Summary
//
//  Created by Daniel García on 27/06/14.
//  Copyright (c) 2014 Produkt. All rights reserved.
//

#import "MovieTableViewCell.h"

@implementation MovieTableViewCell

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
