//
//  MovieDetailViewController.m
//  ShowsMoviesWeek4Summary
//
//  Created by Daniel García on 29/06/14.
//  Copyright (c) 2014 Produkt. All rights reserved.
//

#import "MovieDetailViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "Movie.h"

@interface MovieDetailViewController ()
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *posterImageView;
@property (strong,nonatomic) Movie *movie;
@end

@implementation MovieDetailViewController

- (instancetype)initWithMovie:(Movie *)movie
{
    self = [self initWithNibName:nil bundle:nil];
    if (self) {
        _movie = movie;
        self.title = movie.movieTitle;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self drawMovieView];
}

- (void)drawMovieView{
    self.titleLabel.text = self.movie.movieTitle;
    [self.posterImageView setImageWithURL:[NSURL URLWithString:self.movie.posterURL] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
        
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
