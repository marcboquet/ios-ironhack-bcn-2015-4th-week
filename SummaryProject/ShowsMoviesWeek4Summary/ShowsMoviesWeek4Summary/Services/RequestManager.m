//
//  NetworkService.m
//  ShowsMoviesWeek4Summary
//
//  Created by Daniel García on 27/06/14.
//  Copyright (c) 2014 Produkt. All rights reserved.
//

#import "RequestManager.h"
#import <AFNetworking/AFHTTPRequestOperationManager.h>

@implementation RequestManager
- (instancetype)init
{
    self = [super init];
    if (self) {
        _baseDomain=@"http://api.trakt.tv";
    }
    return self;
}
- (NSOperation<OperationWithDispatchGroup> *)GET:(NSString *)path parameters:(id)parameters successBlock:(RequestManagerSuccess)successBlock errorBlock:(RequestManagerError)errorBlock{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *urlString = [self.baseDomain stringByAppendingPathComponent:path];
    AFHTTPRequestOperation *requestOperation = [manager GET:urlString parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        successBlock(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        errorBlock(error);
    }];
    return (NSOperation<OperationWithDispatchGroup> *)requestOperation;
}

@end
