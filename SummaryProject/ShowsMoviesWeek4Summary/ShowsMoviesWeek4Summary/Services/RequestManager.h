//
//  NetworkService.h
//  ShowsMoviesWeek4Summary
//
//  Created by Daniel García on 27/06/14.
//  Copyright (c) 2014 Produkt. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^RequestManagerSuccess)(id data);
typedef void (^RequestManagerError)(NSError *error);

@protocol OperationWithDispatchGroup <NSObject>
@property (nonatomic, strong) dispatch_group_t completionGroup;
@end

@interface RequestManager : NSObject
@property (copy,nonatomic) NSString *baseDomain;
- (NSOperation<OperationWithDispatchGroup> *)GET:(NSString *)path parameters:(id)parameters successBlock:(RequestManagerSuccess)successBlock errorBlock:(RequestManagerError)errorBlock;
@end
